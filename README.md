Cvičenia z funkcionálneho programovania
=======================================


Cviká
-----

  * Stv, 14:50 I-H3
  * Marián Horňák, sysel(at)ksp.sk
  * [**Zdrojové kódy**](https://bitbucket.org/syslo/fpro-cvika/src/master/materials)
        

Linky
-----

### Kurz
  * [Oficiálna stránka predmetu](http://dai.fmph.uniba.sk/courses/FPRO/)
  * [LIST (systém na odovzdávanie úloh)](http://capek.ii.fmph.uniba.sk/list/)

### Materiály
  * [Sloníková knižka](http://learnyouahaskell.com/chapters/)
  * [Funkcionálne perly](http://dai.fmph.uniba.sk/courses/FPRO/bird_pearls.pdf)

### Haskell
  * [Hoogle](https://www.haskell.org/hoogle/)
  * [Wiki](https://wiki.haskell.org/)

### Prostredia
  * [WinHugs](http://winhugs.software.informer.com/download/)
  * [GHC](https://www.haskell.org/downloads)


Osnova
------

### Cviko 1

  * Inštalácia prostredia
  * Práca s interpretrom
  * Základné dátové typy (boolean, čísla, znaky, n-tice, zoznamy)
  * Písanie vlastného haskell programu
  * Jednoduché funkcie (`doubleMe`, `factorial`)
  * List comprehension

### Cviko 2

  * Štruktúra zoznamu
  * Zoznamová rekurzia (`sum`, `length`)
  * Implementácia základných funkcií na zoznamoch (`sum`, `reverse`, `concat`)
  * Cvičenie práce s listami na polynómoch
  * Základy typového systému

### Cviko 3A

  * Náročnejšie funkcie na zoznamoch (`subsets`, `sort`)
  * Vytvorenie vlastného dátového typu (`data`) a jeho patternmatching

### Cviko 3B

  * Práca s vlastným dátovým typom a stromovou štruktúrou (logický výraz)

### Cviko 4

  * Lambda výrazy
  * Vlastný dátový typ `LExp`
