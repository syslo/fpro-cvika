module Helpers where

-- `subs l` vráti zoznam všetký podzoznamov `l`
-- vzniknutých vynechaním niektorých prvkov
subs :: [a] -> [[a]]
subs l = []

-- Hádaj, čo robím ...
sort :: Ord a => [a] -> [a]
sort l = l

-- `nodups l` vynechá všetky duplikáty v utriedenom zozname `l`
nodups :: Ord a => [a] -> [a]
nodups l = l
