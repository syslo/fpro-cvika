import HelpersSolved

-- Typ logického výrazu, s fungujúcim porovnaním
data Expr = FALSE | TRUE | VAR Char | NOT Expr | AND Expr Expr deriving (Eq)

-- Typ ohodnotenia premenných (funkcia, ktorá každej premennej priradí hodotu)
type Valuation = Char -> Bool


-- `_OR e1 e2` vráti disjunkciu výrazov `e1` a `e2`,
-- vyjadrenú pomocou dostupných konštruktorov.
_OR :: Expr -> Expr -> Expr
_OR e1 e2 = NOT (AND (NOT e1) (NOT e2))

-- `_IMPL e1 e2` vráti implikáciu výrazov `e1` a `e2`,
-- vyjadrenú pomocou dostupných konštruktorov.
_IMPL :: Expr -> Expr -> Expr
_IMPL e1 e2 = NOT (AND e1 (NOT e2))

-- Kto chce alebo potrebuje, môže si dorobiť ďalšie spojky


-- `show e` skonvertuje `e` na string
-- (napísané tak, aby to interpreter používal)
instance Show Expr
	where
		show FALSE = "0"
		show TRUE = "1"
		show (VAR c) = [c]
		show (NOT e) = '~':(show e)
		show (AND e1 e2) = "(" ++ (show e1) ++ " & " ++ (show e2) ++ ")"

-- `simplify e` zjednoduší výraz e (ak sa dá), napríklad
--   - odstránením dvojitého NOT
--   - vyhodnotením konštantných výrazov
--   - vyhodnotením polokonštantného AND
simplify :: Expr -> Expr
simplify FALSE = FALSE
simplify TRUE = TRUE
simplify (VAR c) = (VAR c)
simplify (NOT e) = simplifyNOT (simplify e)
	where
		simplifyNOT FALSE = TRUE
		simplifyNOT TRUE = FALSE
		simplifyNOT (NOT e) = e
		simplifyNOT e = (NOT e)
simplify (AND e1 e2) = simplifyAND (simplify e1) (simplify e2)
	where
		simplifyAND TRUE e2 = e2
		simplifyAND e1 TRUE = e1
		simplifyAND _ FALSE = FALSE
		simplifyAND FALSE _ = FALSE
		simplifyAND e1 e2 = (AND e1 e2)

-- `variables e` vráti unikátny zoznam premenných vo výraze `e`
variables :: Expr -> [Char]
variables = nodups . sort . variables'
	where
		variables' FALSE = []
		variables' TRUE = []
		variables' (VAR c) = [c]
		variables' (NOT e) = variables' e
		variables' (AND e1 e2) = (variables' e1) ++ (variables' e2)

-- `list2val vs` vráti ohodnotenie,
-- v ktorom sú pravdivé iba pre premenné z `vs`
list2val :: [Char] -> Valuation
list2val vs v = elem v vs

-- `eval h e` vráti hodnotu výrazu `e` v ohodnotení `h`
eval :: Valuation -> Expr -> Bool
eval h FALSE = False
eval h TRUE = True
eval h (VAR c) = h c
eval h (NOT e) = not (eval h e)
eval h (AND e1 e2) = (eval h e1) && (eval h e2)

-- `tautology e` je predikát určujúci, či je výraz `e` tautológia
tautology :: Expr -> Bool
tautology e = and [eval h e | h <- map list2val (subs (variables e))]


-- TESTS --

exprs =
	[
		FALSE,
		TRUE,
		VAR 'a',
		AND (VAR 'a') (VAR 'b'),
		_OR (VAR 'a') (VAR 'b'),
		_IMPL (VAR 'a') (VAR 'b'),
		_OR (AND (_IMPL (VAR 'b') (VAR 'c')) (FALSE)) (NOT (VAR 'a')),
		AND (_IMPL (VAR 'a') (VAR 'b')) (_IMPL (VAR 'b') (VAR 'a')),
		_IMPL (VAR 'a') (_IMPL (VAR 'b') (VAR 'a'))
	]

putValuated e vs =
	do
		putStr $ show vs
		putStr " -> "
		putStrLn $ show $ eval (list2val vs) e


putExpr e =
	do
		putStr "Expression: "
		putStrLn $ show e
		putStr "Simplified: "
		putStrLn $ show $ e'
		putStr "Tautology: "
		putStrLn $ show $ tautology e'
		sequence (map (putValuated e') (subs (variables e')))
		putStrLn ""
	where
		e' = simplify e


main = sequence (map putExpr exprs)
