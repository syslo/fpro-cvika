import Helpers

-- Typ logického výrazu, s fungujúcim porovnaním
data Expr = FALSE | TRUE | VAR Char | NOT Expr | AND Expr Expr deriving (Eq)

-- Typ ohodnotenia premenných (funkcia, ktorá každej premennej priradí hodotu)
type Valuation = Char -> Bool


-- `_OR e1 e2` vráti disjunkciu výrazov `e1` a `e2`,
-- vyjadrenú pomocou dostupných konštruktorov.
_OR :: Expr -> Expr -> Expr
_OR e1 e2 = FALSE

-- `_IMPL e1 e2` vráti implikáciu výrazov `e1` a `e2`,
-- vyjadrenú pomocou dostupných konštruktorov.
_IMPL :: Expr -> Expr -> Expr
_IMPL e1 e2 = FALSE

-- Kto chce alebo potrebuje, môže si dorobiť ďalšie spojky


-- `show e` skonvertuje `e` na string
-- (napísané tak, aby to interpreter používal)
instance Show Expr
	where
		show e = "Neviem zobrazit vyraz :("

-- `simplify e` zjednoduší výraz e (ak sa dá), napríklad
--   - odstránením dvojitého NOT
--   - vyhodnotením konštantných výrazov
--   - vyhodnotením polokonštantného AND
simplify :: Expr -> Expr
simplify e = e

-- `variables e` vráti unikátny zoznam premenných vo výraze `e`
variables :: Expr -> [Char]
variables e = []

-- `list2val vs` vráti ohodnotenie,
-- v ktorom sú pravdivé iba pre premenné z `vs`
list2val :: [Char] -> Valuation
list2val vs = \_ -> False

-- `eval h e` vráti hodnotu výrazu `e` v ohodnotení `h`
eval :: Valuation -> Expr -> Bool
eval h e = False

-- `tautology e` je predikát určujúci, či je výraz `e` tautológia
tautology :: Expr -> Bool
tautology e = False


-- TESTS --

exprs =
	[
		FALSE,
		TRUE,
		VAR 'a',
		AND (VAR 'a') (VAR 'b'),
		_OR (VAR 'a') (VAR 'b'),
		_IMPL (VAR 'a') (VAR 'b'),
		_OR (AND (_IMPL (VAR 'b') (VAR 'c')) (FALSE)) (NOT (VAR 'a')),
		AND (_IMPL (VAR 'a') (VAR 'b')) (_IMPL (VAR 'b') (VAR 'a')),
		_IMPL (VAR 'a') (_IMPL (VAR 'b') (VAR 'a'))
	]

putValuated e vs =
	do
		putStr $ show vs
		putStr " -> "
		putStrLn $ show $ eval (list2val vs) e


putExpr e =
	do
		putStr "Expression: "
		putStrLn $ show e
		putStr "Simplified: "
		putStrLn $ show $ e'
		putStr "Tautology: "
		putStrLn $ show $ tautology e'
		sequence (map (putValuated e') (subs (variables e')))
		putStrLn ""
	where
		e' = simplify e


main = sequence (map putExpr exprs)