-----------------------------
---- PREDPRIPRAVENÁ ČASŤ ----
-----------------------------

-- `subs l` vráti zoznam všetký podzoznamov `l`
-- vzniknutých vynechaním niektorých prvkov
subs :: [a] -> [[a]]
subs [] = [[]]
subs (x:xs) = rest ++ (map (x:) rest) where rest = subs xs

-- Hádaj, čo robím ...
sort :: Ord a => [a] -> [a]
sort [] = []
sort (x:xs) = (sort $ filter (<x) xs) ++ [x] ++ (sort $ filter (>=x) xs)

-- `nodups l` vynechá všetky duplikáty v utriedenom zozname `l`
nodups :: Ord a => [a] -> [a]
nodups [] = []
nodups [x] = [x]
nodups (x1:x2:xs)
	| x1 == x2 = nodups (x2:xs)
	| x1 /= x2 = x1:(nodups (x2:xs))

-- Typ logického výrazu, s fungujúcim porovnaním
data Expr = FALSE | TRUE | VAR Char | NOT Expr | AND Expr Expr deriving (Eq)

-- Typ ohodnotenia premenných (funkcia, ktorá každej premennej priradí hodotu)
type Valuation = Char -> Bool

-- `show e` skonvertuje `e` na string
-- (napísané tak, aby to interpreter používal)
instance Show Expr
	where
		show FALSE = "0"
		show TRUE = "1"
		show (VAR c) = [c]
		show (NOT e) = '~':(show e)
		show (AND e1 e2) = "(" ++ (show e1) ++ " & " ++ (show e2) ++ ")"

-- `simplify e` zjednoduší výraz e (ak sa dá), napríklad
--   - odstránením dvojitého NOT
--   - vyhodnotením konštantných výrazov
--   - vyhodnotením polokonštantného AND
simplify :: Expr -> Expr
simplify FALSE = FALSE
simplify TRUE = TRUE
simplify (VAR c) = (VAR c)
simplify (NOT e) = simplifyNOT (simplify e)
	where
		simplifyNOT FALSE = TRUE
		simplifyNOT TRUE = FALSE
		simplifyNOT (NOT e) = e
		simplifyNOT e = (NOT e)
simplify (AND e1 e2) = simplifyAND (simplify e1) (simplify e2)
	where
		simplifyAND TRUE e2 = e2
		simplifyAND e1 TRUE = e1
		simplifyAND _ FALSE = FALSE
		simplifyAND FALSE _ = FALSE
		simplifyAND e1 e2 = (AND e1 e2)

-- `variables e` vráti unikátny zoznam premenných vo výraze `e`
variables :: Expr -> [Char]
variables = nodups . sort . variables'
	where
		variables' FALSE = []
		variables' TRUE = []
		variables' (VAR c) = [c]
		variables' (NOT e) = variables' e
		variables' (AND e1 e2) = (variables' e1) ++ (variables' e2)

-- `list2val vs` vráti ohodnotenie,
-- v ktorom sú pravdivé iba pre premenné z `vs`
list2val :: [Char] -> Valuation
list2val vs v = elem v vs

-- `eval h e` vráti hodnotu výrazu `e` v ohodnotení `h`
eval :: Valuation -> Expr -> Bool
eval h FALSE = False
eval h TRUE = True
eval h (VAR c) = h c
eval h (NOT e) = not (eval h e)
eval h (AND e1 e2) = (eval h e1) && (eval h e2)

-- `tautology e` je predikát určujúci, či je výraz `e` tautológia
tautology :: Expr -> Bool
tautology e = and [eval h e | h <- map list2val (subs (variables e))]

-----------------------------
----   SAMOSTATNÁ ČASŤ   ----
-----------------------------

-- `semEq e1 e2` zistí, či sú `e1` a `e2` sémanticky rovné
semEq :: Expr -> Expr -> Bool
semEq e1 e2 = False

-- `rightNormalForm e` upraví výraz e na ekvivalentný výraz taký,
-- že na žiadny jeho podvýraz nemožno aplikovať `rightTransform`
rightNormalForm :: Expr -> Expr
rightNormalForm e = e
	where rightTransform (AND (AND e1 e2) e3) = AND e1 (AND e2 e3)
