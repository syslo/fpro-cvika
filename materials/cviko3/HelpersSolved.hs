module HelpersSolved where

-- `subs l` vráti zoznam všetký podzoznamov `l`
-- vzniknutých vynechaním niektorých prvkov
subs :: [a] -> [[a]]
subs [] = [[]]
subs (x:xs) = rest ++ (map (x:) rest) where rest = subs xs

-- Hádaj, čo robím ...
sort :: Ord a => [a] -> [a]
sort [] = []
sort (x:xs) = (sort $ filter (<x) xs) ++ [x] ++ (sort $ filter (>=x) xs)

-- `nodups l` vynechá všetky duplikáty v utriedenom zozname `l`
nodups :: Ord a => [a] -> [a]
nodups [] = []
nodups [x] = [x]
nodups (x1:x2:xs)
	| x1 == x2 = nodups (x2:xs)
	| x1 /= x2 = x1:(nodups (x2:xs))
