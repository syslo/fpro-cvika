-- NEKONEČNÉ ZOZNAMY
-- Haskell sa nebojí nekonečných zoznamov. Pracuje s nimi úplne prirodzene:
--     take 10 [3,7..]            = [3,7,11,15,19,23,27,31,35,39]
--     [3,7..] !! 11              = 47
--     head (tail (tail [3,7..])) = 11
--     length [3,7..]             = ... -- nikdy neskonci!

-- `ones` nekonečný zoznam jednotiek
ones = 1:ones

-- `naturalsFrom i` vráti nekonečný zoznam [i, i+1, ...]
-- nepoužívajte vstavané haskelovské generátory!
naturalsFrom i = i:(naturalsFrom (i+1))

-- `naturals` je nekonečný zoznam [0, 1, ...]
-- nepoužívajte vstavané haskelovské generátory!
naturals  = 0:[x+1|x<-naturals]
naturals' = naturalsFrom 0

-------------------------------------------------------------------------------

-- `power2` je nekonečný zoznam všetkých mocnín dvojky
power2    = 1:[x+x | x<-power2]
power2'   = [2^x | x<-[0..]]
power2''  = iterate (\x->2*x) 1
power2''' = map (\x->2^x) [1..]

-- `power3` je nekonečný zoznam všetkých mocnín trojky
power3 = 1:[3*x | x<-power3]

-- `powers n` vráti nekonečný zoznam všetkých mocnín vstupu `n`
powers n    = 1:[n*x | x<-(powers n)]

-- `merge xs ys` spojí dva utriedené (nekonečné) zoznamy bez duplikátov
-- do jedného utriedeného zoznamu bez duplikátov.
merge [] xs = xs
merge xs [] = xs
merge (x:xs) (y:ys)
    | x<y    =  x:(merge xs (y:ys))
    | x==y    =  x:(merge xs ys)
    | otherwise = y:(merge (x:xs) ys)

-- `hamming` je zoznam utriedených prvkov množiny M takej, že:
--   * `1` je v M,
--   * ak je `x` v M, potom aj `2*x` a `3*x` sú v M
hamming = 1:(merge (map (*2) hamming)
            (merge (map (*3) hamming)
                   (map (*5) hamming)))

-------------------------------------------------------------------------------

-- `fibsFrom a b` vráti Fibonacciho postupnosť začínajúcu členmi `a` a `b`
fibsFrom a b = a:(fibsFrom b (b+a))

-- `fibs` vráti Fibonacciho postupnosť začínajúcu členmi 0 a 1
fibs   = 0:1:(map (uncurry (+)) (zip fibs (tail fibs)))
fibs'  = 0:1:(zipWith (+) fibs' (tail fibs'))
fibs'' = fibsFrom 0 1

-- `pascal` vráti zoznam riadkov Pascalovho trojuholníka
--     pascal = [[1], [1,1], [1,2,1], [1,3,3,1], ... ]
pascal  :: [[Int]]
pascal  = [1]:[ map (\(x,y)->x+y) (zip (row ++ [0]) (0:row))| row <-pascal]
pascal' = [1]:[zipWith (+) (row ++ [0]) (0:row) | row <-pascal]

-------------------------------------------------------------------------------

-- `tuples` je zoznam všetkých dvojíc prirodzených čísel,
-- v ktorom sa každá dvojica objaví na konečnom indexe!
tuples = [(i, sucet-i) | sucet <-[0..], i<-[0..sucet]]
