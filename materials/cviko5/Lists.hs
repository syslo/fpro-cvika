-- NEKONEČNÉ ZOZNAMY
-- Haskell sa nebojí nekonečných zoznamov. Pracuje s nimi úplne prirodzene:
--     take 10 [3,7..]            = [3,7,11,15,19,23,27,31,35,39]
--     [3,7..] !! 11              = 47
--     head (tail (tail [3,7..])) = 11
--     length [3,7..]             = ... -- nikdy neskonci!

-- `ones` nekonečný zoznam jednotiek
ones = []

-- `naturalsFrom i` vráti nekonečný zoznam [i, i+1, ...]
-- nepoužívajte vstavané haskelovské generátory!
naturalsFrom i = []

-- `naturals` je nekonečný zoznam [0, 1, ...]
-- nepoužívajte vstavané haskelovské generátory!
naturals = []

-------------------------------------------------------------------------------

-- `power2` je nekonečný zoznam všetkých mocnín dvojky
power2 = []


-- `power3` je nekonečný zoznam všetkých mocnín trojky
power3 = []

-- `powers n` vráti nekonečný zoznam všetkých mocnín vstupu `n`
powers n = []

-- `merge xs ys` spojí dva utriedené (nekonečné) zoznamy bez duplikátov
-- do jedného utriedeného zoznamu bez duplikátov.
merge xs ys = []

-- `hamming` je zoznam utriedených prvkov množiny M takej, že:
--   * `1` je v M,
--   * ak je `x` v M, potom aj `2*x` a `3*x` sú v M
hamming = []

-------------------------------------------------------------------------------

-- `fibsFrom a b` vráti Fibonacciho postupnosť začínajúcu členmi `a` a `b`
fibsFrom a b = []

-- `fibs` vráti Fibonacciho postupnosť začínajúcu členmi 0 a 1
fibs = []

-- `pascal` vráti zoznam riadkov Pascalovho trojuholníka
--     pascal = [[1], [1,1], [1,2,1], [1,3,3,1], ... ]
pascal  :: [[Int]]
pascal = []

-------------------------------------------------------------------------------

-- `tuples` je zoznam všetkých dvojíc prirodzených čísel,
-- v ktorom sa každá dvojica objaví na konečnom indexe!
tuples = []
