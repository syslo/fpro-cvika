module Terms where

import Types

-- `subterms l` vráti zoznam všetkých podtermov termu `l`
subterms :: LExp -> [LExp]
subterms l = []

-- `free l` vráti zoznam všetkých premenných,
-- ktoré majú voľný výskyt v terme `l`
free :: LExp -> [Var]
free l = []

-- `bound l` vráti zoznam všetkých premenných,
-- ktoré majú viazaný výskyt v terme `l`
bound :: LExp -> [Var]
bound l = []

-- `substitute v k l` substituuje všetky voľné výskyty `v`
-- v terme `l` za `k`
substitute :: Var -> LExp -> LExp -> LExp
substitute v k l = l

-- `oneStepBetaReduce l` spraví nejaké beta redukcie v `l`
-- podľa Vami zvolenej stratégie
oneStepBetaReduce :: LExp -> LExp
oneStepBetaReduce l = l

-- `normalForm l` iteruje `oneStepBetaReduce` na `l`, kým sa mení
normalForm :: LExp -> LExp
normalForm l = l
