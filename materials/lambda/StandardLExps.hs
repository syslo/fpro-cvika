module StandardLExps where

import Types


-- Definícia a funkcie pracujúce so zoznamom lambda výrazov.
-- Výhodou je, že ak sa rozhodneme reprezentovať LExpList pomocou niečoho
-- iného, stačí zmeniť túto časť, netreba meniť všetko.

type LExpList = [(String, LExp)]

emptyLExpList :: LExpList
emptyLExpList = []

addToLExpList :: String -> LExp -> LExpList -> LExpList
addToLExpList name lExp list = (name, lExp):list

getFromLExpList :: LExpList -> String -> LExp
getFromLExpList list name = snd $ head $ filter ((name ==) . fst) list

enumLExpList :: LExpList -> [(String, LExp)]
enumLExpList = id

standardLExps :: LExpList
standardLExps =  customLExpsAdder $ globalLExpsAdder $ emptyLExpList

getStandard :: String -> LExp
getStandard = getFromLExpList standardLExps


-- definícia všeobecných výrazov

globalLExpsAdder = id
	.(addToLExpList "I" (LAMBDA "x" (ID "x")))
	.(addToLExpList "K" (LAMBDA "x" (LAMBDA "y" (ID "x"))))
	.(addToLExpList "S" (LAMBDA "x" (LAMBDA "y" (LAMBDA "z" (APP (APP (ID "x") (ID "z")) (APP (ID "y") (ID "z")))))))


-- definícia používateľových vlastných výrazov

customLExpsAdder = id
	-- Tu si môžete písať vlastné štandardné výrazy
	-- .(addToLExpList "VlastneMeno" (LAMBDA "x" (ID "x")))

	-- Koniec vlastných výrazov
	