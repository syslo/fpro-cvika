module Tests where

import Types
import Terms
import StandardLExps


-- Kód testov (zatiaľ mu nemusíte rozumieť)

test = do
	testTerms
	testReduces

testTerms = sequence (map testTerm termTests) 
testReduces = sequence (map testReduce reduceTests) 

termTests = enumLExpList $ termTestLExpAdder $ emptyLExpList
reduceTests = enumLExpList $ reduceTestLExpAdder $ emptyLExpList

testTerm (name, lExp) = do
	putStr "Term "
	putStr name
	putStr ": "
	putStrLn $ show lExp
	putStr "Subterms: "
	putStrLn $ show $ subterms lExp
	putStr "Free variables: "
	putStrLn $ show $ free lExp
	putStrLn ""

testReduce (name, lExp) = do
	putStr "Reduce of "
	putStr name
	putStr ": "
	putStrLn $ show lExp
	putStr "is: "
	putStrLn $ show $ normalForm lExp
	putStrLn ""


-- Dáta testov

termTestLExpAdder = id
	.customLExpsAdder
	.globalLExpsAdder
	-- Tu si môžete písať ďalšie testy pre subterms, free, bound:
	-- .(addToLExpList "VlastneMeno" (LAMBDA "x" (ID "x")))

	-- Koniec vlastných testov

reduceTestLExpAdder = id
	.(addToLExpList "R0" (APP (LAMBDA "x" (ID "z")) (ID "y")))
	.(addToLExpList "R1" (APP (getStandard "I") (getStandard "I")))
	.(addToLExpList "R2" (APP (getStandard "K") (getStandard "I")))
	.(addToLExpList "R3" (APP (APP (getStandard "K") (getStandard "I")) (getStandard "S")))
	.(addToLExpList "R4" (APP (APP (getStandard "S") (getStandard "K")) (getStandard "K")))
	-- Tu si môžete písať ďalšie testy pre normalForm:
	-- .(addToLExpList "VlastneMeno" (LAMBDA "x" (ID "x")))

	-- Koniec vlastných testov