module Types where

type Var = String

data LExp = LAMBDA Var LExp | ID Var | APP LExp LExp
	deriving(Show, Eq)

data Ski = S | K | I | APL Ski Ski
