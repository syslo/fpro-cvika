module Ski where

import Types

-- konvertuje LExp do Ski
toSki :: LExp -> Ski
toSki _ = I

-- konvertuje Ski do LExp
toLExp :: Ski -> LExp
toLExp _ = LAMBDA "x" (ID "x")

-- zredukuje Ski
reduceSki :: Ski -> Ski
reduceSki _ = I
