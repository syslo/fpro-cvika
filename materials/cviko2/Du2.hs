-- FPRO 2015, šablóna domácej úlohy 2


-- Naprogramujte v Haskelli funkcie skipeven a skipodd,
-- ktoré zo zoznamu vynechajú každý druhý prvok.
skipeven l = []
skipodd l = []


-- Naprogramujte v Haskelli funkciu zipmerge,
-- ktorá spojí dva zoznamy do jedného,
-- v ktorom sa budú prvky pôvodných zoznamov striedať.
zipmerge l1 l2 = [] 


-- Naprogramujte v Haskelli funkciu max2nd,
-- ktorá vráti druhý najväčší prvok zo zoznamu.
-- Ak má zoznam menej ako 2 prvky, správanie si dodefinujte lubovoľne.
max2nd (a:as) = a



examples =
	[
		skipeven "abcdefg" == "aceg",
		skipodd "abcdefg" == "bdf",

		zipmerge "abcdef" "xyz" == "axbyczdef",
		zipmerge "xyz" "abcdef" == "xaybzcdef",

		max2nd "palacinka" == 'n',
		max2nd [7,8,1,2,5,1,3] == 7,
		max2nd [7,8,1,2,8,2,4] == 8
	]
