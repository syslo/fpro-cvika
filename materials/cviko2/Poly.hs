-- FPRO 2015, Zadanie samostatnej úlohy na cvičení 2


-- Typ pre polynóm. List koeficientov začínajúc najnižším.
type Poly a = [a]


-- `polyFunc p x` vráti hodnotu polynómu `p` v bode `x`
polyFunc :: Num a => Poly a -> a -> a
polyFunc p x = 0


-- `polySum p1 p2` vráti súčet polynómov `p1` a `p2`
polySum :: Num a => Poly a -> Poly a -> Poly a
polySum p1 p2 = []


-- `polyProd p1 p2` vráti súčin polynómov `p1` a `p2`
polyProd :: Num a => Poly a -> Poly a -> Poly a
polyProd p1 p2 = []


-- `polyShow p` vráti textovú reprezentáciu `p`
polyShow :: Show a => Poly a -> String
polyShow p = ""


-- `polyDerivate p` vráti deriváciu `p`
polyDerivate :: Num a => Poly a -> Poly 
polyDerivate p = []
