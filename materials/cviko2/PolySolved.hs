-- FPRO 2015, Zadanie samostatnej úlohy na cvičení 2


-- Typ pre polynóm. List koeficientov začínajúc najnižším.
type Poly a = [a]


-- `polyFunc p x` vráti hodnotu polynómu `p` v bode `x`
polyFunc :: Num a => Poly a -> a -> a
polyFunc [] x = 0
polyFunc (a:as) x = a + x*(polyFunc as x)


-- `polySum p1 p2` vráti súčet polynómov `p1` a `p2`
polySum :: Num a => Poly a -> Poly a -> Poly a
polySum p1 [] = p1
polySum [] p2 = p2
polySum (a:as) (b:bs) = (a+b):(polySum as bs)

-- `polyProd p1 p2` vráti súčin polynómov `p1` a `p2`
polyProd :: Num a => Poly a -> Poly a -> Poly a
polyProd [] p = []
polyProd (a:as) p = polySum (map (a*) p) (0:(polyProd as p))


-- `polyShow p` vráti textovú reprezentáciu `p`
polyShow :: Show a => Poly a -> String
polyShow p = showHelp p 0
	where
		showHelp [] 0 = "0"
		showHelp [] i = ""
		showHelp (a:as) 0 =  (show a) ++ (showHelp as 1)
		showHelp (a:as) i =  " + " ++ (show a) ++ "x^" ++ (show i) ++ (showHelp as (i+1))

-- `polyDerivate p` vráti deriváciu `p`
polyDerivate :: Num a => Poly a -> Poly a
polyDerivate [] = []
polyDerivate (_:p) = derivateHelp p 1
	where
		derivateHelp [] _ = []
		derivateHelp (a:as) i = (a*i):(derivateHelp as (i+1))
