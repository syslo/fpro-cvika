-- FPRO 2015, šablóna domácej úlohy 2


-- Naprogramujte v Haskelli funkcie skipeven a skipodd,
-- ktoré zo zoznamu vynechajú každý druhý prvok.
skipeven [] = []
skipeven (a:as) = a:(skipodd as)
skipodd [] = []
skipodd (a:as) = (skipeven as)


-- Naprogramujte v Haskelli funkciu zipmerge,
-- ktorá spojí dva zoznamy do jedného,
-- v ktorom sa budú prvky pôvodných zoznamov striedať.
zipmerge [] bs = bs
zipmerge (a:as) bs = a:(zipmerge bs as)


-- Naprogramujte v Haskelli funkciu max2nd,
-- ktorá vráti druhý najväčší prvok zo zoznamu.
-- Ak má zoznam menej ako 2 prvky, správanie si dodefinujte lubovoľne.
max2nd (a1:a2:as)
	| a1 > a2   = helper as a1 a2
	| otherwise = helper as a2 a1
	where
		helper [] _ m2 = m2
		helper (a:as) m1 m2
			| a > m1    = helper as a m1
			| a > m2    = helper as m1 a
			| otherwise = helper as m1 m2



examples =
	[
		skipeven "abcdefg" == "aceg",
		skipodd "abcdefg" == "bdf",

		zipmerge "abcdef" "xyz" == "axbyczdef",
		zipmerge "xyz" "abcdef" == "xaybzcdef",

		max2nd "palacinka" == 'n',
		max2nd [7,8,1,2,5,1,3] == 7,
		max2nd [7,8,1,2,8,2,4] == 8
	]
